export declare type CreateTransfer = {
	origin_id: string,
    to: {
        label: string
    },
    value: {
        currency: string,
        amount: number
    },
    description: string,
    concept: string,
    emails: [
       string
    ]
};