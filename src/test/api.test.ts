import { expect } from 'chai';
import * as _ from 'lodash';
import * as nock from 'nock';
import { URL } from '../constants';
import Bind from '../bind';

const BIND = new Bind({ user: 'estradapablo@gmail.com', pass: '4QfOvgj4NFvlHpO' });
const TOKEN = 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJQV2tLUTA3UW8vcHVKSDBjZzJvVVdVQ003Nncwb1J3WXN0VW1sY0FrcnpnPSIsImNyZWF0ZWQiOjE1ODkyOTgzNDgzMDYsIm5hbWUiOiJQYWJsbyBFc3RyYWRhIiwiZXhwIjoxNTg5MzI3MTQ4fQ.8YfIYwcomFVz_vxj6Nf3IEbCAyjGonRbuU13sopHL2-oxuJdKa9JaCCYPZ8g09ilEl7mMJqFYI-5_R5XAP-y5w';

describe('Bind API', () => {
	beforeEach(() => {
		nock.cleanAll();
	});

	it('Make a Login', async () => {

		nock(URL)
			.post('/login/jwt', {
				"username": "estradapablo@gmail.com",
				"password": "4QfOvgj4NFvlHpO"
			})
			.reply(200, {
				"token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJQV2tLUTA3UW8vcHVKSDBjZzJvVVdVQ003Nncwb1J3WXN0VW1sY0FrcnpnPSIsImNyZWF0ZWQiOjE1ODkyOTM2NTg5MTIsIm5hbWUiOiJQYWJsbyBFc3RyYWRhIiwiZXhwIjoxNTg5MzIyNDU4fQ.YCD7HktGsPmRRDc2JFfk5j2uwHmR1qvyym-a_uZ760NNk7soOu_IU4ygVKQ4Ig39mwG9pGCuy3pFcVm5UyD-vg",
				"expires_in": 28800
			})

		const result = await BIND.login();

		expect(result).not.to.have.property('error');
		expect(result).to.have.property('token');
	});


	it('Accounts View', async () => {

		nock(URL)
			.get('/banks/322/accounts')
			.reply(200, [
				{
					"id": "21-1-99999-4-6",
					"label": null,
					"bank_id": "322",
					"views_available": [
						{
							"id": "owner",
							"shortName": "Owner",
							"public": false
						}
					]
				}
			])

		const result = await BIND.accountsView(322, TOKEN);
		expect(result).not.to.have.property('error');
		expect(result.length).equal(1);
		expect(result[0]).to.have.property('id');
		expect(result[0]).to.have.property('bank_id');
		expect(result[0]).to.have.property('views_available');
		expect(result[0].views_available.length).equal(1);
	});


	it('Accounts List', async () => {

		nock(URL)
			.get('/banks/322/accounts/owner')
			.reply(200, [
				{
					"id": "21-1-99999-4-6",
					"label": "BOCHA.ASTRO.NUEZ",
					"number": "99999",
					"type": "Caja de Ahorro",
					"status": "NORMAL",
					"owners": [
						{
							"id": "27876543212",
							"display_name": "COORPORACION CAPSULA",
							"id_type": "1",
							"is_physical_person": true
						}
					],
					"balance": {
						"currency": "2",
						"amount": 197801
					},
					"bank_id": "322",
					"account_routing": {
						"scheme": "CBU",
						"address": "3220001822000055910031"
					}
				},
				{
					"id": "21-1-99999-2-6",
					"label": "BOCHA.ASTRO.NUEZ",
					"number": "99999",
					"type": "Caja de Ahorro",
					"status": "NORMAL",
					"owners": [
						{
							"id": "27876543212",
							"display_name": "COORPORACION CAPSULA",
							"id_type": "1",
							"is_physical_person": true
						}
					],
					"balance": {
						"currency": "80",
						"amount": 104882.6
					},
					"bank_id": "322",
					"account_routing": {
						"scheme": "CBU",
						"address": "3220001823000055910025"
					}
				}
			])

		const result = await BIND.accountsList(322, 'owner', TOKEN);
		expect(result).not.to.have.property('error');
		expect(result.length).equal(2);
	});

	it('Account Details', async () => {

		nock(URL)
			.get('/banks/322/accounts/21-1-99999-4-6/owner')
			.reply(200, {
				"id": "21-1-99999-4-6",
				"label": null,
				"number": "99999",
				"type": "Caja de Ahorro",
				"status": "NORMAL",
				"owners": [
					{
						"id": "27876543212",
						"display_name": "COORPORACION CAPSULA",
						"id_type": "1",
						"is_physical_person": true
					}
				],
				"balance": {
					"currency": "2",
					"amount": 197801
				},
				"bank_id": "322",
				"account_routing": {
					"scheme": "CBU",
					"address": "3220001822000055910031"
				}
			})

		const result = await BIND.accountDetails(322, 'owner', '21-1-99999-4-6', TOKEN);
		expect(result).not.to.have.property('error');
		expect(result).to.have.property('id');
		expect(result).to.have.property('type');
		expect(result).to.have.property('balance');
		expect(result).to.have.property('bank_id');
		expect(result.owners.length).equal(1);
	});


	it('Account Transactions', async () => {

		nock(URL)
			.get('/banks/322/accounts/21-1-99999-4-6/owner/transactions')
			.reply(200, require('./data/transactions.json'))

		const result = await BIND.accountTransactions(322, 'owner', '21-1-99999-4-6', TOKEN);
		expect(result).not.to.have.property('error');
		expect(result.length).equal(144);
	});

	it('Account Transfers', async () => {

		nock(URL)
			.get('/banks/322/accounts/21-1-99999-4-6/owner/transaction-request-types/TRANSFER')
			.reply(200, [
				{
					"id": "1-20309792166-015114433092974-0",
					"type": "TRANSFER",
					"from": {
						"bank_id": "322",
						"account_id": "21-1-99999-4-6"
					},
					"counterparty": {
						"id_type": "UNAVAILABLE",
						"bank_routing": {
							"scheme": "UNAVAILABLE",
							"address": ""
						},
						"account_routing": {
							"scheme": "LABEL",
							"address": "AliasPrueba1234"
						}
					},
					"details": {
						"origin_id": "55789",
						"type": "TRANSFERENCIAS_ENVIADAS"
					},
					"transaction_ids": [
						"1-20309792166-015114433092974-0"
					],
					"status": "COMPLETED",
					"start_date": "2020-05-12T11:45:07.485-03:00",
					"end_date": "2020-05-12T11:45:07.485-03:00",
					"charge": {
						"summary": "VAR COMPLETE_TRANS",
						"value": {
							"currency": "ARS",
							"amount": 10
						}
					}
				}
			])

		const result = await BIND.accountTransfers(322, 'owner', '21-1-99999-4-6', TOKEN);
		expect(result).not.to.have.property('error');
		expect(result.length).equal(1);
	});

	it('Account Transfer', async () => {

		nock(URL)
			.get('/banks/322/accounts/21-1-99999-4-6/owner/transaction-request-types/TRANSFER/1-20309792166-015114433092974-0')
			.reply(200, {
				"id": "1-20309792166-015114433092974-0",
				"type": "TRANSFER",
				"from": {
					"bank_id": "322",
					"account_id": "21-1-99999-4-6"
				},
				"counterparty": {
					"id_type": "UNAVAILABLE",
					"bank_routing": {
						"scheme": "UNAVAILABLE",
						"address": ""
					},
					"account_routing": {
						"scheme": "LABEL",
						"address": "AliasPrueba1234"
					}
				},
				"details": {
					"origin_id": "55789"
				},
				"transaction_ids": [
					"1-20309792166-015114433092974-0"
				],
				"status": "COMPLETED",
				"start_date": "2020-05-12T11:45:07.485-03:00",
				"end_date": "2020-05-12T11:45:07.485-03:00",
				"charge": {
					"summary": "VAR COMPLETE_TRANS",
					"value": {
						"currency": "ARS",
						"amount": 10
					}
				}
			})

		const result = await BIND.accountTransfer(322, 'owner', '21-1-99999-4-6', '1-20309792166-015114433092974-0', TOKEN);
		expect(result).not.to.have.property('error');
		expect(result).to.have.property('id');
		expect(result).to.have.property('type');
	});

	it('Create Transfer', async () => {

		nock(URL)
			.post('/banks/322/accounts/21-1-99999-4-6/owner/transaction-request-types/TRANSFER/transaction-requests')
		    .reply(200, {
				"id": "1-20309792166-015114433092975-0",
				"type": "TRANSFER",
				"from": {
					"bank_id": "322",
					"account_id": "21-1-99999-4-6"
				},
				"counterparty": {
					"id_type": "UNAVAILABLE",
					"bank_routing": {
						"scheme": "UNAVAILABLE",
						"address": ""
					},
					"account_routing": {
						"scheme": "LABEL",
						"address": "AliasPrueba1234"
					}
				},
				"details": {
					"origin_id": "557890"
				},
				"transaction_ids": [
					"1-20309792166-015114433092975-0"
				],
				"status": "COMPLETED",
				"start_date": "2020-05-12T14:07:53.303-03:00",
				"end_date": "2020-05-12T14:07:53.303-03:00",
				"charge": {
					"summary": "VAR COMPLETE_TRANS",
					"value": {
						"currency": "ARS",
						"amount": 101
					}
				}
			})

		const result = await BIND.createTransfer(322, 'owner', '21-1-99999-4-6', {
			'origin_id': '557890',
			'to': {
				'label': 'AliasPrueba1234'
			},
			'value': {
				'currency': 'ARS',
				'amount': 101
			},
			'description': 'COMPLETE_TRANS',
			'concept': 'VAR',
			'emails': [
				'apibank@poincenot.com'
			]
		}, TOKEN);
		expect(result).not.to.have.property('error');
		expect(result).to.have.property('id');
		expect(result).to.have.property('status');
	});
});

