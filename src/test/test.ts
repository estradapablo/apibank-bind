import * as debug from 'debug';
import Bind from '../bind';

const Debug = debug('test');
const bind = new Bind({ user: 'estradapablo@gmail.com', pass: '4QfOvgj4NFvlHpO' });

bind.createTransfer(322, 'owner', '21-1-99999-4-6', {
    origin_id: 55788,
    to: {
        label: "AliasPrueba12345"
    },
    value: {
        currency: "ARS",
        amount: "10"
    },
    description: "COMPLETE_TRANS nueva",
    concept: "VAR",
    emails: [
        "apibank@poincenot.com"
    ]
},null)
  .then((result) => {
    Debug('Account Transfer then');
    Debug(JSON.stringify(result), 'resultados');

  });