import * as debug from 'debug';
import Bind from '../bind';

const Debug = debug('test');
const bind = new Bind({ user: 'estradapablo@gmail.com', pass: '4QfOvgj4NFvlHpO' });


bind.login()
  .then((result) => {
    Debug('Login then');
    Debug(JSON.stringify(result), 'resultados');

  });


bind.accountsView(322, null)
  .then((result) => {
    Debug('Accounts View then');
    Debug(JSON.stringify(result), 'resultados');

  });

bind.accountsList(322, 'owner', null)
  .then((result) => {
    Debug('Acounts List then');
    Debug(JSON.stringify(result), 'resultados');

  });

bind.accountDetails(322, 'owner', '21-1-99999-4-6', null)
  .then((result) => {
    Debug('Account Fetail then');
    Debug(JSON.stringify(result), 'resultados');

  });

bind.accountTransactions(322, 'owner', '21-1-99999-4-6', null)
  .then((result) => {
    Debug('Account Transactions then');
    Debug(result.length, 'resultados');

  });


bind.accountTransfers(322, 'owner', '21-1-99999-4-6', null)
  .then((result) => {
    Debug('Accounts Transfers then');
    Debug(JSON.stringify(result), 'resultados');

  });

bind.accountTransfer(322, 'owner', '21-1-99999-4-6', '1-20309792166-015114433092974-0', null)
  .then((result) => {
    Debug('Account Transfer then');
    Debug(JSON.stringify(result), 'resultados');

  });


bind.createTransfer(322, 'owner', '21-1-99999-4-6', {
  origin_id: 55788,
  to: {
    label: "AliasPrueba12345"
  },
  value: {
    currency: "ARS",
    amount: "10"
  },
  description: "COMPLETE_TRANS nueva",
  concept: "VAR",
  emails: [
    "apibank@poincenot.com"
  ]
}, null)
  .then((result) => {
    Debug('Account Transfer then');
    Debug(JSON.stringify(result), 'resultados');

  }); 