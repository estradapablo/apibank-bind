import 'mocha';
import { expect } from 'chai';
import * as QueryBuilder from '../query-builder';

describe('Bind query builder', () => {
  it('Login', () => {
    expect(QueryBuilder.login('estradapablo@gmail.com', '4QfOvgj4NFvlHpO')).eql({
      username: 'estradapablo@gmail.com',
      password: '4QfOvgj4NFvlHpO'
    });
  });

  it('Create Transfer', () => {
    expect(QueryBuilder.crateTransfer({
			'origin_id': '557890',
			'to': {
				'label': 'AliasPrueba1234'
			},
			'value': {
				'currency': 'ARS',
				'amount': 101
			},
			'description': 'COMPLETE_TRANS',
			'concept': 'VAR',
			'emails': [
				'apibank@poincenot.com'
			]
		})).eql({
			'origin_id': '557890',
			'to': {
				'label': 'AliasPrueba1234'
			},
			'value': {
				'currency': 'ARS',
				'amount': 101
			},
			'description': 'COMPLETE_TRANS',
			'concept': 'VAR',
			'emails': [
				'apibank@poincenot.com'
			]
		});
  });

});
