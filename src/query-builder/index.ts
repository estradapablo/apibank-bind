import {CreateTransfer} from '../helpers/types/types'

export function login(user: string, pass: string): any {
	
	let body = {
		username: user,
		password: pass
	}

	return body;
};


export function crateTransfer(query: any): any {

	let body: CreateTransfer = {
		origin_id: query.origin_id,
		to: {
			label: query.to.label
		},
		value: {
			currency: query.value.currency,
			amount: query.value.amount
		},
		description: query.description,
		concept: query.concept,
		emails: [
			query.emails[0]
		]
	}

	return body;
};