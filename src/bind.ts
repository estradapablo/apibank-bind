import * as debug from 'debug';
import { EventEmitter } from 'events';
import axios from 'axios';

import { URL } from './constants';
import * as queryBuilder from './query-builder';


const Debug = require('debug')('providers:bind');

export default class Bind extends EventEmitter {
    user: any;
    pass: any;
    url: any;

    constructor(credentials) {
        super();

        const { user, pass } = credentials;

        if (!user || !pass) {
            throw new Error('Bind: missing auth');
        }

        this.user = user
        this.pass = pass
        this.url = URL

    }

    async login(): Promise<any> {
        const body = queryBuilder.login(this.user, this.pass);
        const url = '/login/jwt'
        const result = await this.fetch(url, 'POST', body, null);
        if (result.token) {
            return result
        } else {
            Debug(`Login sin Token`);
            throw new Error('BIND, error al generar token');
        }

    }

    async accountsView(bank_id: number, token: string): Promise<any> {

        if (!token) {
            let loginResponse = await this.login()
            token = loginResponse.token
        }

        const url = '/banks/' + bank_id + '/accounts'
        const result = await this.fetch(url, 'GET', null, token);

        return result
    }

    async accountsList(bank_id: number, view_id: string, token: string): Promise<any> {

        if (!token) {
            let loginResponse = await this.login()
            token = loginResponse.token
        }

        const url = '/banks/' + bank_id + '/accounts/' + view_id
        const result = await this.fetch(url, 'GET', null, token);

        return result
    }

    async accountDetails(bank_id: number, view_id: string, account_id: string, token: string): Promise<any> {

        if (!token) {
            let loginResponse = await this.login()
            token = loginResponse.token
        }

        const url = '/banks/' + bank_id + '/accounts/' + account_id + '/' + view_id
        const result = await this.fetch(url, 'GET', null, token);

        return result
    }


    async accountTransactions(bank_id: number, view_id: string, account_id: string, token: string): Promise<any> {

        if (!token) {
            let loginResponse = await this.login()
            token = loginResponse.token
        }

        const url = '/banks/' + bank_id + '/accounts/' + account_id + '/' + view_id + '/transactions'
        const result = await this.fetch(url, 'GET', null, token);

        return result
    }

    async accountTransfers(bank_id: number, view_id: string, account_id: string, token: string): Promise<any> {

        if (!token) {
            let loginResponse = await this.login()
            token = loginResponse.token
        }

        const url = '/banks/' + bank_id + '/accounts/' + account_id + '/' + view_id + '/transaction-request-types/TRANSFER'
        const result = await this.fetch(url, 'GET', null, token);

        return result
    }

    async accountTransfer(bank_id: number, view_id: string, account_id: string, transaction_id: string, token: string): Promise<any> {

        if (!token) {
            let loginResponse = await this.login()
            token = loginResponse.token
        }

        const url = '/banks/' + bank_id + '/accounts/' + account_id + '/' + view_id + '/transaction-request-types/TRANSFER/' + transaction_id
        const result = await this.fetch(url, 'GET', null, token);

        return result
    }

    async createTransfer(bank_id: number, view_id: string, account_id: string, query: object, token: string): Promise<any> {
        /***
         * Aca tendria que ir la logica del query, dependiendo de como venga desde la app que lo mande o bien omitir esto y mandar el request directo con lo que venga
         * ***/
        const body = queryBuilder.crateTransfer(query);

        if (!token) {
            let loginResponse = await this.login()
            token = loginResponse.token
        }

        const url = '/banks/' + bank_id + '/accounts/' + account_id + '/' + view_id + '/transaction-request-types/TRANSFER/transaction-requests'
        const result = await this.fetch(url, 'POST', body, token);

        return result
    }

    async fetch(action: string, method: string, body?: object, token?: string) {
        Debug(`Request ${action} bind`);
        let start: any = new Date();

        try {

            const url = this.url + action

            Debug(`${action} - ${url}`);

            this.emit('request', {
                body,
                action,
                delay: 0,
            });

            const options: any = {
                url,
                method,
                keepAlive: false,
                headers: {
                    'Content-Type': 'application/json',
                },
                gzip: true,
                timeout: 30000
            };

            if (method === 'POST') {
                options.data = body;
            }

            if (token) {
                options.headers.Authorization = 'JWT ' + token;
            }

            let { data: result } = await axios(options);

            Debug(`Response ${action}`);

            this.emit('response', {
                action,
                result,
                delay: (<any>new Date() - start) / 1000,
            });

            if (!result) {
                Debug(`Response ${action} sin respuesta`);
                throw new Error('BIND, respuesta vacia');
            }


            return result;

        } catch (err) {
            Debug(`Response ${action} Bind error ${err.message}`);
            Debug(err.stack);

            this.emit('custom_error', {
                action,
                error: err.message,
                delay: (<any>new Date() - start) / 1000,
            });

            return { error: err.message };
        }
    }
}