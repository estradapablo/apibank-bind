# README #

APIBANK - BIND INTEGRATION

### Installing

```
yarn
```

or

```
npm install
```

## Running the tests

yarn test

yarn live-all (For live test)

### End to end tests

  Bind API
    ✓ Make a Login
    ✓ Accounts View
    ✓ Accounts List
    ✓ Account Details
    ✓ Account Transactions
    ✓ Account Transfers
    ✓ Account Transfer
    ✓ Create Transfer

  Bind query builder
    ✓ Login
    ✓ Create Transfer


  10 passing (46ms)

Done in 0.75s.

## Deployment

yarn build

## Built With

* [NodeJs](https://nodejs.org/es/docs/)
* [chai] (https://www.chaijs.com/api/) - For Test

## Authors

* **Pablo Estrada** - *Initial work* - [Pablo Estrada](https://bitbucket.org/estradapablo/)